# wordle.js-hawaiian

Hawaiian Clone of the wordle game at:
https://www.powerlanguage.co.uk/wordle/
Wordlist from http://crubadan.org/languages/haw
Spoiler warning: knowing the order of the wordlist means you know what comes next.


## About the code and sources
The original code was "Copyright (c) Microsoft Corporation" but licensed with permission to change and copy. It was released on the mentioned site in a 'minimized' form. This version has been 'beautified' with indentation. Beyond that the wordlist, keyboard and copyright has been changed (to add my own). Same license or 'Unlicense'.

The wordlist I got and is embedded in the code is a modified version from:
http://crubadan.org/languages/haw which is provided under CC-BY-4.0
I filtered on word length (5 characters) and removed words with #, @ or - in it.

I am not Hawaiian and do not speak the language, I have not been able to (yet) verify the accuracy of the list and welcome very much any suggestions to changes to this list or replacement lists.

## Usage

Place in a directory and point your browser at it. You only need the javascript and html files.

